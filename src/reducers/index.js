import { combineReducers } from "redux";
import videoReducer from "./videoReducer";
import screenReducer from "./screenReducer";

export default combineReducers({
  videos: videoReducer,
  backgrounds: videoReducer,
  screen: screenReducer
});
