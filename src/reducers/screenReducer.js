import { SCREEN_CHOICE } from "../actions/types";

const initState = {
  screen: ""
};

export default function(state = initState, action) {
  switch (action.type) {
    case SCREEN_CHOICE:
      console.log("SCREEN_CHOICE");
      return {
        ...state,
        screen: action.payload
      };
    default:
      return state;
  }
}
