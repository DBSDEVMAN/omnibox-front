import { FETCH_VIDEOS, FETCH_BACKGROUNDS } from "../actions/types";

const initState = {
  videos: [],
  backgrounds: [],
  screen: ""
};

export default function(state = initState, action) {
  switch (action.type) {
    case FETCH_VIDEOS:
      return {
        ...state,
        videos: action.payload
      };
    case FETCH_BACKGROUNDS:
      return {
        ...state,
        backgrounds: action.payload
      };
    default:
      return state;
  }
}
