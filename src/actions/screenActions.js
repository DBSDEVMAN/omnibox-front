import { SCREEN_CHOICE } from "./types";

export const screenChoice = screenChoice => dispatch => {
  console.log("in screenAction", screenChoice);
  dispatch({
    type: SCREEN_CHOICE,
    payload: screenChoice
  });
};
