import React from "react";
import { Link } from "react-router-dom";

const VideoTypes = props => {
  console.log(props);
  return (
    <div>
      <div className="center container">
        <h1>What would you like to cast?</h1>
        <div className="center btn-group container">
          <Link to="/videos">
            <button className="green-background-left inset-box-shadow-2">
              Videos
            </button>
          </Link>
          <Link to="/background-videos">
            <button className="green-background-left inset-box-shadow-2">
              Text Over a Video Background
            </button>
          </Link>
          <Link to="/url">
            <button className="green-background-left inset-box-shadow-2">
              Your Own URL
            </button>
          </Link>
        </div>

        <div className="btn-group" />
      </div>
    </div>
  );
};

export default VideoTypes;
