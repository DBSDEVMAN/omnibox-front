import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchBackgrounds } from "../actions/backgroundActions";

class BackgroundVideos extends React.Component {
  componentDidMount() {
    this.props.fetchBackgrounds();
  }

  handleClick = e => {
    const uri = "http://127.0.0.1:4000/video";
    const id = e.uri.replace("/videos/", "");
    axios
      .post(uri, {
        id,
        style: `#text_tint{background:radial-gradient(ellipse at center,rgba(0,0,0,.75) 0,rgba(0,0,0,0) 100%);z-index:2;position:absolute;top:0;left:0;width:100vw;height:100vh}#person_message{position:fixed;top:70px;left:65px;z-index:100000;width:calc(100vw - 200px);height:calc(100vh - 200px);color:#cecece;font-family:"Open Sans";text-align:center;line-height:1em;font-size:2em;border:8px solid #fff;background:linear-gradient(45deg,rgba(237,237,237,1) 0,rgba(246,246,246,1) 53%,rgba(255,255,255,1) 100%);box-shadow:0 2px 2px rgba(0,0,0,.075),0 4px 4px rgba(0,0,0,.075),0 8px 8px rgba(0,0,0,.075),0 16px 16px rgba(0,0,0,.075);padding:30px;display:flex;flex-direction:column;align-items:center;justify-content:center;transition:all 1s ease-in-out;transform-origin:center center}#person_message.flip{transform:rotateY(360deg)}#person_message h1{font-size:2.5em;line-height:1em;font-weight:700;text-transform:uppercase;letter-spacing:-.075em;margin:0;color:#538b3f;font-weight:100}#person_name{font-size:6em;letter-spacing:-.025em;color:#595959;font-family:"Open Sans Condensed"}#person_name>*{line-height:1em;display:block}`,
        text_overlay: `<div id=person_message><h1>Welcome</h1><div id=person_name><span>Test</span> <span>Text</span></div></div><div id=text_tint></div>`,
        scripts: `window.addEventListener("DOMContentLoaded",function(e){var n=document.getElementById("person_message");setInterval(function(){n.classList.toggle("flip")},1500)});`
      })
      .then(function(res) {
        console.log(
          res,
          `Congrats! You casted ${e.name} to a whole 'nother screen!`
        );
      });
  };

  render() {
    const backgrounds = this.props.videos;
    const videoList = backgrounds.length ? (
      backgrounds.map(video => {
        return (
          <div key={video.uri.replace("/videos/", "")}>
            <Link to="/confirm" onClick={e => this.handleClick(video)}>
              <div className="col s12 m7">
                <div className="card horizontal green-text">
                  <div className="card-image">
                    <img
                      src={video.pictures.sizes[3].link}
                      className="card-height"
                      alt={video.name}
                    />
                  </div>
                  <div className="card-stacked">
                    <div className="card-content">
                      <h3>{video.name}</h3>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          </div>
        );
      })
    ) : (
      <div className="center">Loading...</div>
    );
    return (
      <div className="container">
        <h1 className="center">These videos look great with text over them:</h1>
        {videoList}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  videos: state.backgrounds.backgrounds
});

export default connect(
  mapStateToProps,
  { fetchBackgrounds }
)(BackgroundVideos);
