import React from "react";
import axios from "axios";

class ChooseUrl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      url: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const uri = "http://127.0.0.1:4000/url";
    const url = this.state.url;
    axios.post(uri, { url }).then(function(res) {
      console.log(res);
    });
    this.setState({
      value: ""
    });
  }

  render() {
    return (
      <div className="center container">
        <form onSubmit={this.handleSubmit}>
          <label>
            <h2>What URL would you like to use?</h2>
            <input
              type="text"
              value={this.state.value}
              onChange={this.handleChange}
              autoFocus
            />
          </label>
          <input
            className="green-background-left inset-box-shadow-2 center btn-group container"
            type="submit"
            value="Cast!"
          />
        </form>
      </div>
    );
  }
}

export default ChooseUrl;
