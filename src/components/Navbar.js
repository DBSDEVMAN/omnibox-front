import React from "react";
import logo from "../assets/img/daugherty_logo.svg";
import team from "../assets/img/teamdaugherty.png";
import { Link } from "react-router-dom";

const Navbar = props => {
  return (
    <header className="green-background inset-box-shadow-2">
      <Link to="/">
        <img className="team" src={team} alt="Team Daugherty" />
      </Link>
      <img src={logo} alt="Daugherty Logo" />
    </header>
  );
};

export default Navbar;
