import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchVideos } from "../actions/videoActions";

class Videos extends React.Component {
  componentDidMount() {
    this.props.fetchVideos();
  }

  handleClick = e => {
    const uri = "http://127.0.0.1:4000/video";
    const id = e.uri.replace("/videos/", "");
    axios
      .post(uri, {
        id,
        style: "",
        text_overlay: "",
        scripts: ""
      })
      .then(function(res) {
        console.log(
          res,
          `Congrats! You casted ${e.name} to a whole 'nother screen!`
        );
      });
  };

  render() {
    const videos = this.props.videos;
    // console.log(videos);
    const videoList = videos.length ? (
      videos.map(video => {
        return (
          <div key={video.uri.replace("/videos/", "")}>
            <Link to="/confirm" onClick={e => this.handleClick(video)}>
              <div className="col s12 m7">
                <div className="card horizontal green-text">
                  <div className="card-image">
                    <img
                      src={video.pictures.sizes[3].link}
                      className="card-height"
                      alt={video.name}
                    />
                  </div>
                  <div className="card-stacked">
                    <div className="card-content">
                      <h3>{video.name}</h3>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          </div>
        );
      })
    ) : (
      <div className="center">Loading...</div>
    );
    return (
      <div>
        <h1 className="center">These videos look great on their own:</h1>
        <div className="container">{videoList}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  videos: state.videos.videos
});

export default connect(
  mapStateToProps,
  { fetchVideos }
)(Videos);
