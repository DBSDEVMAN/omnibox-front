import React from "react";
import { Link } from "react-router-dom";
import Screens from "../assets/helpers/ScreenList";
import tv from "../assets/img/Blank_television_set.svg.png";
import { screenChoice } from "../actions/screenActions";
import { connect } from "react-redux";

class ScreenChooser extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...this.state, Screens };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = e => {
    this.props.screenChoice(e);
    console.log("handle click", e);
    // e.preventDefault();
  };

  render() {
    const screenList = this.state.Screens.screenList.map(screen => {
      return (
        <div key={screen.value}>
          <Link to="/videoTypes" onClick={e => this.handleClick(screen)}>
            <li className="collection-item avatar">
              <img src={tv} alt="cool, awesome tv" />
              <span className="title ">{screen.label}</span>
            </li>
          </Link>
          <br />
          <hr />
          <br />
        </div>
      );
    });
    return (
      <div>
        <h1 className="center">Here are your screen options:</h1>
        <hr />
        <br />
        <ul className="collection container">{screenList}</ul>
      </div>
    );
  }
}

export default connect(
  null,
  { screenChoice }
)(ScreenChooser);
