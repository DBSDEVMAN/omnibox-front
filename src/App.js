import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import VideoTypes from "./components/VideoTypes";
import Navbar from "./components/Navbar";
import ChooseUrl from "./components/ChooseUrl";
import Videos from "./components/Videos";
import BackgroundVideos from "./components/BackgroundVideos";
import "./assets/styles/base_structure.scss";
import Confirmation from "./components/Confirmation";
import ScreenChooser from "./components/ScreenChooser";
import { Provider } from "react-redux";
import store from "./store";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className="app">
            <Navbar />
            <div className="spacer">
              <Route exact path="/" component={ScreenChooser} />
              <Route path="/videoTypes" component={VideoTypes} />
              <Route path="/url" component={ChooseUrl} />
              <Route path="/videos" component={Videos} />
              <Route path="/background-videos" component={BackgroundVideos} />
              <Route path="/confirm" component={Confirmation} />
            </div>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
