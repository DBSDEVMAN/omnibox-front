const screenList = [
  { value: "stl_11_lobby", label: "STL - 11 - Lobby" },
  { value: "atl_lobby", label: "ATL - Lobby" },
  { value: "atl_kitchen", label: "ATL - Kitchen" },
  { value: "atl_devCenter", label: "ATL - Dev Center" }
];

const screenChoice = "";

const screenState = {
  screenList,
  screenChoice
};

export default screenState;
