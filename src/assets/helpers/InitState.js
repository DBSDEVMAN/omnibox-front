import screenList from "./ScreenList";

const initState = {
  screenList,
  screenChoice: "",
  videos: "",
  backgrounds: ""
};

export default initState;
