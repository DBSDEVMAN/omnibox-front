## Project Description:

This project is designed to allow users to cast media from this app to a Linux-based system and display it on that system's screen (similar to a chromecast). To do this, this front end app sends data (video, overlay text, URL, etc.) to a [backend app](https://bitbucket.org/DBSDEVMAN/omnibox-back/src/master/), which assembles a html file from the data or makes sure the URL is viable. ~~It then sends the file (via FTP) to the Linux system, followed by a command (via SSH) to display it in Chromium in 'Kiosk' (full screen) mode.~~ The file or address is then pulled by the client to display on screen.

The app is generally pretty simple and intuitive. The architecture is _currently_ arranged as follows:

ScreenChooser: User will have to declare to which screen they wish to cast the video.
VideoTypes: The user declares what sort of media they wish to cast (video on its own, video with text overlay, or URL), which will lead them to Videos, BackgroundVideos, or ChooseUrl

This information is then sent to the backend where it is prepared for casting and the Confirmation screen is displayed to the user.

## In Progress and Next Steps:

Dashboard component: Another 'video type,' this component is already designed and on the app developed by the team in Atlanta, and just needs to be integrated into this project.

State management: There is not a need for a very large and indepth state management system. It will be used primarily to hold the user's choice of screens and possibly preferences for text overlay and dashboard data.

User Login: This will be tied to SSO/AD and required for all users. In addition to a security layer, it will be tied to the list of screens and limit the user's choices to what they are authorized to use.

## Available Scripts

In the project directory, you can run:

### `npm i`

Installs all packages necessary for the app to run.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
